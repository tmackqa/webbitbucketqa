Feature: 
   Sign In using Long email formats

  Scenario: 
    Created Account with <format> for its email address can Sign In

        Given member has navigated to Website Sign In page with valid account and using supported browser
        When member enters Email "email" and Password "p4y+y39Ir5PEPmX20UxFKw=="
        Then member gains access to member portals
      | email                                                                                             | format                                                                  |
      | qatestemailcharactercountallowercase@mailinator.com                                               | 50+ characters all lower case                                           |
      | QATESTEMAILCHARACTERCOUNTALLUPPERCASE@MAILINATOR.COM                                              | 50+ characters all Upper case                                           |
      | qATestEmailCHARACTERCOUNallMIXedcase@MAILInATOR.cOM                                               | 50+ characters all MIXed case                                           |
      | qATestEmailCHARACTERCOUNTMIXedcaseAndSupportedSpeciacharacters!#$%&'*+-/=?^_@MAILInATOR.cOM       | 50+ characters all MIXed case and supported Special characters          |
      | qATestEmailCHARACTERCOUNTMIXedcaseAndSupportedSpeciacharactersDigits!#$%&'*+-/=?^_@MAILInATOR.cOM | 90+ characters all MIXed case, supported Special characters, and Digits |
    Then member gains access to member portals
