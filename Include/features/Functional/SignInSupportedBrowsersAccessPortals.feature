Feature: Sign In to Website

    Scenario: Member is able to Sign In using supported Browsers and access portals
        Given member has valid account and using supported browser
        And has navigated to Website Sign In page
        When member enters Email "QA_677700_9A60F016@Mailinator.com" and Password "p4y+y39Ir5PEPmX20UxFKw=="
        Then member gains access to member portals
