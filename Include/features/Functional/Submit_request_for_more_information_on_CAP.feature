@WEB-6
@Automation

Feature: Submit request for more information on CAP

  Scenario: Lead for CAP completes form requesting more information
    Given lead has navigated to CAP certification page
    When lead completes More Information CAP form and submits
    Then request form is sent for CAP, lead receives Thank You message
