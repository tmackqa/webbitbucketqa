@WEB-6
@Automation

Feature: Submit request for more information on SSCP

  Scenario: Lead for SSCP completes form requesting more infomation
    Given lead has navigated to SSCP certification page
    When lead completes More Information SSCP form and submits
    Then request form is sent for SSCP, lead receives Thank You message