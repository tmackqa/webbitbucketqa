
Feature: Submit request for more information on CISSP

@test

  Scenario: Lead for CISSP completes form requesting more information
    Given lead has navigated to CISSP certification page
    When lead completes More Information CISSP form and submits
    Then request form is sent for CISSP, lead receives Thank You message
