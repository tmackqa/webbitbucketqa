package functional
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When


class Submit_request_for_more_information_on_CAP {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@Given("lead has navigated to CAP certification page")
	def Inavigate_to_CAP_form_request() {
		WebUI.openBrowser('')
		WebUI.navigateToUrl('https://wwwqa.isc2.org/Certifications/CAP')
	}

	@When("lead completes More Information CAP form and submits")
	def completrequest_for_more_info_CAP_submitted() {
		WebUI.setText(findTestObject('Object Repository/Forms/CAP/Page_Security Authorization Certifi/input_First Name_wffm79e626365'),
				'QAAutomationCAPLeadFN')
		WebUI.setText(findTestObject('Object Repository/Forms/CAP/Page_Security Authorization Certifi/input_Last Name_wffm79e6263659'),
				'QAAutomationCAPLeadLN')
		WebUI.setText(findTestObject('Object Repository/Forms/CAP/Page_Security Authorization Certifi/input_Company_wffm79e6263659e0'),
				'QAAutomationCAPLeadCompany')
		WebUI.setText(findTestObject('Object Repository/Forms/CAP/Page_Security Authorization Certifi/input_Email_wffm79e6263659e040'),
				'QAAutomationCAPLead@mailinator.com')
		WebUI.setText(findTestObject('Object Repository/Forms/CAP/Page_Security Authorization Certifi/input_Phone Number_wffm79e6263'),
				'813-123-4567')
		WebUI.selectOptionByValue(findTestObject('Object Repository/Forms/CAP/Page_Security Authorization Certifi/select_CountryAfghanistanAland'),
				'United States', true)
		WebUI.selectOptionByValue(findTestObject('Object Repository/Forms/CAP/Page_Security Authorization Certifi/select_Select Training OptionO'),
				'Official Individual Training For You', true)
		WebUI.click(findTestObject('Object Repository/Forms/CAP/Page_Security Authorization Certifi/input_spageurl_btn  btn-defaul'))
	}

	@Then("request form is sent for CAP, lead receives Thank You message")
	def sent_request_for_more_Info_CAP_and_recieve_Thank_You_message() {
		WebUI.closeBrowser()
	}
}