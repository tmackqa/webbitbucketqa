package functional
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.apache.commons.validator.EmailValidator
import org.eclipse.jetty.util.security.Password
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When


class SignInSupportedBrowsersAccessPortals {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@Given("member has valid account and using supported browser")
	def member_has_valid_account_and_using_supported_browser() {
		WebUI.openBrowser('')
	}

	@And("has navigated to Website Sign In page")
	def I_navigated_to_Website_and_click_signin() {
		WebUI.navigateToUrl('https://wwwqa.isc2.org/')
		WebUI.click(findTestObject('Object Repository/Page_Cybersecurity and IT Security/a_Sign In'))
	}

	@When('member enters Email "(.*)" and Password "(.*)"')
	def member_enters_vaild_Email_and_Password(String Email, String Password) {
		WebUI.setText(findTestObject('Object Repository/Page_/input_Required_Email'), 'QA_677700_9A60F016@Mailinator.com')
		WebUI.setEncryptedText(findTestObject('Object Repository/Page_/input_Required_Password'), 'p4y+y39Ir5PEPmX20UxFKw==')
		WebUI.click(findTestObject('Object Repository/Page_/button_SIGN IN'))
	}

	@Then("member gains access to member portals")
	def member_gains_access_to_system_portals() {
		WebUI.verifyTextPresent('Hi', true)
		WebUI.click(findTestObject('Object Repository/Page_Cybersecurity and IT Security/a_Hi Unnamed'))
		WebUI.click(findTestObject('Object Repository/Page_Cybersecurity and IT Security/a_Sign Out'))
		WebUI.closeBrowser()
	}
}