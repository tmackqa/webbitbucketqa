package functional
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When


class CISSPcompletesformrequestingmoreinfo {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@Given("lead has navigated to CISSP certification page")
	def navigate_to_CISSP_form_request() {
		WebUI.openBrowser('')
		WebUI.navigateToUrl('https://wwwqa.isc2.org/Certifications/CISSP')
	}

	@When("lead completes More Information CISSP form and submits")
	def completrequest_for_more_info_CISSP_submitted() {
		WebUI.setText(findTestObject('Object Repository/Forms/CISSP/Page_Cybersecurity Certification CI/input_First name_wffmf65102418'),
				'QAAutomationCISSPLeadFN')
		WebUI.setText(findTestObject('Object Repository/Forms/CISSP/Page_Cybersecurity Certification CI/input_Last name_wffmf651024180'),
				'QAAutomationCISSPLeadLN')
		WebUI.setText(findTestObject('Object Repository/Forms/CISSP/Page_Cybersecurity Certification CI/input_Phone number_wffmf651024'),
				'813-123-4567')
		WebUI.setText(findTestObject('Object Repository/Forms/CISSP/Page_Cybersecurity Certification CI/input_Email address_wffmf65102'),
				'QAAutomationCISSPLead@mailinator.com')
		WebUI.setText(findTestObject('Object Repository/Forms/CISSP/Page_Cybersecurity Certification CI/input_Company_wffmf65102418010'),
				'QAAutomationCISSPLeadCompany')
		WebUI.selectOptionByValue(findTestObject('Object Repository/Forms/CISSP/Page_Cybersecurity Certification CI/select_CountryAfghanistanAland'),
				'Congo', true)
		WebUI.selectOptionByValue(findTestObject('Object Repository/Forms/CISSP/Page_Cybersecurity Certification CI/select_Select Training OptionO'),
				'Official Group Training For Your Team', true)
		WebUI.click(findTestObject('Object Repository/Forms/CISSP/Page_Cybersecurity Certification CI/input_spageurl_btn  btn-defaul'))
	}

	@Then("request form is sent for CISSP, lead receives Thank You message")
	def sent_request_for_more_Info_CISSP_and_recieve_Thank_You_message() {
		WebUI.waitForPageLoad(0)
		WebUI.verifyElementPresent(findTestObject('Forms/CISSP/CISSP Thank You'), 0)
		WebUI.closeBrowser()
	}
}