$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/tmack/git/Web/Include/features/Functional/SignInSupportedBrowsersAccessPortals.feature");
formatter.feature({
  "name": "Sign In to Website",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Member is able to Sign In using supported Browsers and access portals",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "member has valid account and using supported browser",
  "keyword": "Given "
});
formatter.match({
  "location": "SignInSupportedBrowsersAccessPortals.member_has_valid_account_and_using_supported_browser()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "has navigated to Website Sign In page",
  "keyword": "And "
});
formatter.match({
  "location": "SignInSupportedBrowsersAccessPortals.I_navigated_to_Website_and_click_signin()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "member enters Email \"QA_677700_9A60F016@Mailinator.com\" and Password \"p4y+y39Ir5PEPmX20UxFKw\u003d\u003d\"",
  "keyword": "When "
});
formatter.match({
  "location": "SignInSupportedBrowsersAccessPortals.member_enters_vaild_Email_and_Password(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "member gains access to member portals",
  "keyword": "Then "
});
formatter.match({
  "location": "SignInSupportedBrowsersAccessPortals.member_gains_access_to_system_portals()"
});
formatter.result({
  "error_message": "com.kms.katalon.core.exception.StepFailedException: Unable to verify text \u0027Hi\u0027 is present  using regular expression (Root cause: Text \u0027Hi\u0027 is not present on page  using regular expression)\r\n\tat com.kms.katalon.core.keyword.internal.KeywordMain.stepFailed(KeywordMain.groovy:36)\r\n\tat com.kms.katalon.core.webui.keyword.internal.WebUIKeywordMain.stepFailed(WebUIKeywordMain.groovy:65)\r\n\tat com.kms.katalon.core.webui.keyword.internal.WebUIKeywordMain.runKeyword(WebUIKeywordMain.groovy:27)\r\n\tat com.kms.katalon.core.webui.keyword.builtin.VerifyTextPresentKeyword.verifyTextPresent(VerifyTextPresentKeyword.groovy:83)\r\n\tat com.kms.katalon.core.webui.keyword.builtin.VerifyTextPresentKeyword.execute(VerifyTextPresentKeyword.groovy:68)\r\n\tat com.kms.katalon.core.keyword.internal.KeywordExecutor.executeKeywordForPlatform(KeywordExecutor.groovy:53)\r\n\tat com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords.verifyTextPresent(WebUiBuiltInKeywords.groovy:1693)\r\n\tat com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords$verifyTextPresent$4.call(Unknown Source)\r\n\tat org.codehaus.groovy.runtime.callsite.CallSiteArray.defaultCall(CallSiteArray.java:48)\r\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.call(AbstractCallSite.java:113)\r\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.call(AbstractCallSite.java:133)\r\n\tat functional.SignInSupportedBrowsersAccessPortals.member_gains_access_to_system_portals(SignInSupportedBrowsersAccessPortals.groovy:76)\r\n\tat ✽.member gains access to member portals(C:/Users/tmack/git/Web/Include/features/Functional/SignInSupportedBrowsersAccessPortals.feature:7)\r\n",
  "status": "failed"
});
});