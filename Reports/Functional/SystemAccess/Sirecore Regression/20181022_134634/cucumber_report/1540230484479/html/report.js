$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/tmack/git/Web/Include/features/Functional/CISSPcompletesformrequestingmoreinfo.feature");
formatter.feature({
  "name": "Submit request for more information on CISSP",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Lead for CISSP completes form requesting more infomation",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "lead has navigated to CISSP certification page",
  "keyword": "Given "
});
formatter.match({
  "location": "CISSPcompletesformrequestingmoreinfo.navigate_to_CISSP_form_request()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "lead completes More Information CISSP form and submits",
  "keyword": "When "
});
formatter.match({
  "location": "CISSPcompletesformrequestingmoreinfo.completrequest_for_more_info_CISSP_submitted()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "request form is sent for CISSP, lead receives Thank You message",
  "keyword": "Then "
});
formatter.match({
  "location": "CISSPcompletesformrequestingmoreinfo.sent_request_for_more_Info_CISSP_and_recieve_Thank_You_message()"
});
formatter.result({
  "status": "passed"
});
});