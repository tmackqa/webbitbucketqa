$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/tmack/git/Web/Include/features/Functional/SignInSupportedBrowsersAccessPortals.feature");
formatter.feature({
  "name": "Sign In to Website",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Member is able to Sign In using supported Browsers and access portals",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "member has valid account and using supported browser",
  "keyword": "Given "
});
formatter.match({
  "location": "SignInSupportedBrowsersAccessPortals.member_has_valid_account_and_using_supported_browser()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "has navigated to Website Sign In page",
  "keyword": "And "
});
formatter.match({
  "location": "SignInSupportedBrowsersAccessPortals.I_navigated_to_Website_and_click_signin()"
});
formatter.result({
  "error_message": "com.kms.katalon.core.exception.StepFailedException: Unable to navigate to \u0027https://wwwqa.isc2.org/\u0027 (Root cause: org.openqa.selenium.WebDriverException: Reached error page: about:neterror?e\u003ddnsNotFound\u0026u\u003dhttps%3A//wwwqa.isc2.org/\u0026c\u003dUTF-8\u0026f\u003dregular\u0026d\u003dWe%20can%E2%80%99t%20connect%20to%20the%20server%20at%20wwwqa.isc2.org.\nBuild info: version: \u00273.7.1\u0027, revision: \u00278a0099a\u0027, time: \u00272017-11-06T21:07:36.161Z\u0027\nSystem info: host: \u0027ISC-42LQQF2\u0027, ip: \u0027172.19.0.1\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_181\u0027\nDriver info: com.kms.katalon.core.webui.driver.firefox.CGeckoDriver\nCapabilities {acceptInsecureCerts: true, browserName: firefox, browserVersion: 62.0.2, javascriptEnabled: true, moz:accessibilityChecks: false, moz:headless: false, moz:processID: 8332, moz:profile: C:\\Users\\tmack\\AppData\\Loca..., moz:useNonSpecCompliantPointerOrigin: false, moz:webdriverClick: true, pageLoadStrategy: normal, platform: XP, platformName: XP, platformVersion: 10.0, proxy: Proxy(direct), rotatable: false, timeouts: {implicit: 0, pageLoad: 300000, script: 30000}}\nSession ID: 8295b2b8-8a0f-4b2c-af75-8968efd6abda)\r\n\tat com.kms.katalon.core.keyword.internal.KeywordMain.stepFailed(KeywordMain.groovy:36)\r\n\tat com.kms.katalon.core.webui.keyword.internal.WebUIKeywordMain.stepFailed(WebUIKeywordMain.groovy:65)\r\n\tat com.kms.katalon.core.webui.keyword.internal.WebUIKeywordMain.runKeyword(WebUIKeywordMain.groovy:27)\r\n\tat com.kms.katalon.core.webui.keyword.builtin.NavigateToUrlKeyword.navigateToUrl(NavigateToUrlKeyword.groovy:83)\r\n\tat com.kms.katalon.core.webui.keyword.builtin.NavigateToUrlKeyword.execute(NavigateToUrlKeyword.groovy:67)\r\n\tat com.kms.katalon.core.keyword.internal.KeywordExecutor.executeKeywordForPlatform(KeywordExecutor.groovy:53)\r\n\tat com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords.navigateToUrl(WebUiBuiltInKeywords.groovy:183)\r\n\tat com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords$navigateToUrl$0.call(Unknown Source)\r\n\tat org.codehaus.groovy.runtime.callsite.CallSiteArray.defaultCall(CallSiteArray.java:48)\r\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.call(AbstractCallSite.java:113)\r\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.call(AbstractCallSite.java:125)\r\n\tat functional.SignInSupportedBrowsersAccessPortals.I_navigated_to_Website_and_click_signin(SignInSupportedBrowsersAccessPortals.groovy:63)\r\n\tat ✽.has navigated to Website Sign In page(C:/Users/tmack/git/Web/Include/features/Functional/SignInSupportedBrowsersAccessPortals.feature:5)\r\n",
  "status": "failed"
});
formatter.step({
  "name": "member enters Email \"QA_677700_9A60F016@Mailinator.com\" and Password \"p4y+y39Ir5PEPmX20UxFKw\u003d\u003d\"",
  "keyword": "When "
});
formatter.match({
  "location": "SignInSupportedBrowsersAccessPortals.member_enters_vaild_Email_and_Password(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "member gains access to member portals",
  "keyword": "Then "
});
formatter.match({
  "location": "SignInSupportedBrowsersAccessPortals.member_gains_access_to_system_portals()"
});
formatter.result({
  "status": "skipped"
});
});