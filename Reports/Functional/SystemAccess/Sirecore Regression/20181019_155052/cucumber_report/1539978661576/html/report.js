$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/tmack/git/Web/Include/features/Functional/SignInSupportedBrowsersAccessPortals.feature");
formatter.feature({
  "name": "Sign In to Website",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Member is able to Sign In using supported Browsers and access portals",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "member has valid account and using supported browser",
  "keyword": "Given "
});
formatter.match({
  "location": "SignInSupportedBrowsersAccessPortals.member_has_valid_account_and_using_supported_browser()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "has navigated to Website Sign In page",
  "keyword": "And "
});
formatter.match({
  "location": "SignInSupportedBrowsersAccessPortals.I_navigated_to_Website_and_click_signin()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "member enters Email \"QA_677700_9A60F016@Mailinator.com\" and Password \"p4y+y39Ir5PEPmX20UxFKw\u003d\u003d\"",
  "keyword": "When "
});
formatter.match({
  "location": "SignInSupportedBrowsersAccessPortals.member_enters_vaild_Email_and_Password(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "member gains access to member portals",
  "keyword": "Then "
});
formatter.match({
  "location": "SignInSupportedBrowsersAccessPortals.member_gains_access_to_system_portals()"
});
formatter.result({
  "status": "passed"
});
});