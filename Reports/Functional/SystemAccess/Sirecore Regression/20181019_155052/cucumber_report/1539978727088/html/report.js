$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/tmack/git/Web/Include/features/Functional/Submit_request_for_more_information_on_CAP.feature");
formatter.feature({
  "name": "Submit request for more information on CAP",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@WEB-6"
    },
    {
      "name": "@Automation"
    }
  ]
});
formatter.scenario({
  "name": "Lead for CAP completes form requesting more information",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@WEB-6"
    },
    {
      "name": "@Automation"
    }
  ]
});
formatter.step({
  "name": "lead has navigated to CAP certification page",
  "keyword": "Given "
});
formatter.match({
  "location": "Submit_request_for_more_information_on_CAP.Inavigate_to_CAP_form_request()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "lead completes More Information CAP form and submits",
  "keyword": "When "
});
formatter.match({
  "location": "Submit_request_for_more_information_on_CAP.completrequest_for_more_info_CAP_submitted()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "request form is sent for CAP, lead receives Thank You message",
  "keyword": "Then "
});
formatter.match({
  "location": "Submit_request_for_more_information_on_CAP.sent_request_for_more_Info_CAP_and_recieve_Thank_You_message()"
});
formatter.result({
  "status": "passed"
});
});