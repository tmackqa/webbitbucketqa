$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/tmack/git/Web/Include/features/Functional/Submit_request_for_more_information_on_SSCP.feature");
formatter.feature({
  "name": "Submit request for more information on SSCP",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Lead for SSCP completes form requesting more infomation",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "lead has navigated to SSCP certification page",
  "keyword": "Given "
});
formatter.match({
  "location": "Submit_request_for_more_information_on_SSCP.navigate_to_SSCP_form_request()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "lead completes More Information SSCP form and submits",
  "keyword": "When "
});
formatter.match({
  "location": "Submit_request_for_more_information_on_SSCP.completrequest_for_more_info_SSCP_submitted()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "request form is sent for SSCP, lead receives Thank You message",
  "keyword": "Then "
});
formatter.match({
  "location": "Submit_request_for_more_information_on_SSCP.sent_request_for_more_Info_SSCP_and_recieve_Thank_You_message()"
});
formatter.result({
  "status": "passed"
});
});