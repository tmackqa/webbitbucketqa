$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/tmack/git/Web/Include/features/Functional/SignInSupportedBrowsersAccessPortals.feature");
formatter.feature({
  "name": "Sign In to Website",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Member is able to Sign In using supported Browsers and access portals",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "member has valid account and using supported browser",
  "keyword": "Given "
});
formatter.match({
  "location": "signInAccessPortal.member_has_valid_account_and_using_supported_browser()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "has navigated to Website Sign In page",
  "keyword": "And "
});
formatter.match({
  "location": "signInAccessPortal.I_navigated_to_Website_and_click_signin()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "member enters Email \"QA_677700_9A60F016@Mailinator.com\" and Password \"p4y+y39Ir5PEPmX20UxFKw\u003d\u003d\"",
  "keyword": "When "
});
formatter.match({
  "location": "signInAccessPortal.member_enters_vaild_Email_and_Password(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "member gains access to member portals",
  "keyword": "Then "
});
formatter.match({
  "location": "signInAccessPortal.member_gains_access_to_system_portals()"
});
formatter.result({
  "error_message": "com.kms.katalon.core.exception.StepFailedException: Unable to click on object \u0027Object Repository/Page_Cybersecurity and IT Security/a_Sign Out\u0027 (Root cause: org.openqa.selenium.ElementNotInteractableException: Element \u003ca href\u003d\"/ISC2Logout\"\u003e could not be scrolled into view\nBuild info: version: \u00273.7.1\u0027, revision: \u00278a0099a\u0027, time: \u00272017-11-06T21:07:36.161Z\u0027\nSystem info: host: \u0027ISC-42LQQF2\u0027, ip: \u0027172.19.0.1\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_181\u0027\nDriver info: com.kms.katalon.core.webui.driver.firefox.CGeckoDriver\nCapabilities {acceptInsecureCerts: true, browserName: firefox, browserVersion: 62.0.2, javascriptEnabled: true, moz:accessibilityChecks: false, moz:headless: false, moz:processID: 37008, moz:profile: C:\\Users\\tmack\\AppData\\Loca..., moz:useNonSpecCompliantPointerOrigin: false, moz:webdriverClick: true, pageLoadStrategy: normal, platform: XP, platformName: XP, platformVersion: 10.0, proxy: Proxy(direct), rotatable: false, timeouts: {implicit: 0, pageLoad: 300000, script: 30000}}\nSession ID: 75348a87-9c80-486d-b983-5a6f227f5b12)\r\n\tat com.kms.katalon.core.keyword.internal.KeywordMain.stepFailed(KeywordMain.groovy:36)\r\n\tat com.kms.katalon.core.webui.keyword.internal.WebUIKeywordMain.stepFailed(WebUIKeywordMain.groovy:65)\r\n\tat com.kms.katalon.core.webui.keyword.internal.WebUIKeywordMain.runKeyword(WebUIKeywordMain.groovy:27)\r\n\tat com.kms.katalon.core.webui.keyword.builtin.ClickKeyword.click(ClickKeyword.groovy:86)\r\n\tat com.kms.katalon.core.webui.keyword.builtin.ClickKeyword.execute(ClickKeyword.groovy:67)\r\n\tat com.kms.katalon.core.keyword.internal.KeywordExecutor.executeKeywordForPlatform(KeywordExecutor.groovy:53)\r\n\tat com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords.click(WebUiBuiltInKeywords.groovy:616)\r\n\tat com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords$click$1.call(Unknown Source)\r\n\tat org.codehaus.groovy.runtime.callsite.CallSiteArray.defaultCall(CallSiteArray.java:48)\r\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.call(AbstractCallSite.java:113)\r\n\tat org.codehaus.groovy.runtime.callsite.AbstractCallSite.call(AbstractCallSite.java:125)\r\n\tat functional.signInAccessPortal.member_gains_access_to_system_portals(signInAccessPortal.groovy:78)\r\n\tat ✽.member gains access to member portals(C:/Users/tmack/git/Web/Include/features/Functional/SignInSupportedBrowsersAccessPortals.feature:7)\r\n",
  "status": "failed"
});
});