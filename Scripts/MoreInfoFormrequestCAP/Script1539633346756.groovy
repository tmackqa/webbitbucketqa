import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://wwwqa.isc2.org/Certifications/CAP')

WebUI.setText(findTestObject('Object Repository/Forms/CAP/Page_Security Authorization Certifi/input_First Name_wffm79e626365'), 
    'QAAutomationCAPLeadFN')

WebUI.setText(findTestObject('Object Repository/Forms/CAP/Page_Security Authorization Certifi/input_Last Name_wffm79e6263659'), 
    'QAAutomationCAPLeadLN')

WebUI.setText(findTestObject('Object Repository/Forms/CAP/Page_Security Authorization Certifi/input_Company_wffm79e6263659e0'), 
    'QAAutomationCAPLeadCompany')

WebUI.setText(findTestObject('Object Repository/Forms/CAP/Page_Security Authorization Certifi/input_Email_wffm79e6263659e040'), 
    'QAAutomationCAPLead@mailinator.com')

WebUI.setText(findTestObject('Object Repository/Forms/CAP/Page_Security Authorization Certifi/input_Phone Number_wffm79e6263'), 
    '813-123-4567')

WebUI.selectOptionByValue(findTestObject('Object Repository/Forms/CAP/Page_Security Authorization Certifi/select_CountryAfghanistanAland'), 
    'United States', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Forms/CAP/Page_Security Authorization Certifi/select_Select Training OptionO'), 
    'Official Individual Training For You', true)

WebUI.click(findTestObject('Object Repository/Forms/CAP/Page_Security Authorization Certifi/input_spageurl_btn  btn-defaul'))

WebUI.closeBrowser()

