import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://wwwqa.isc2.org/Certifications/SSCP')

WebUI.setText(findTestObject('Object Repository/Forms/SSCP/Page_IT Security Certification  SSC/input_First name_wffmf65102418'), 
    'QAAutomationSSCPLeadFN')

WebUI.setText(findTestObject('Object Repository/Forms/SSCP/Page_IT Security Certification  SSC/input_Last name_wffmf651024180'), 
    'QAAutomationSSCPLeadLN')

WebUI.setText(findTestObject('Object Repository/Forms/SSCP/Page_IT Security Certification  SSC/input_Phone number_wffmf651024'), 
    '813-123-4567')

WebUI.setText(findTestObject('Object Repository/Forms/SSCP/Page_IT Security Certification  SSC/input_Email address_wffmf65102'), 
    'QAAutomationSSCPLead@mailinator.com')

WebUI.setText(findTestObject('Object Repository/Forms/SSCP/Page_IT Security Certification  SSC/input_Company_wffmf65102418010'), 
    'QAAutomationSSCPLeadCompany')

WebUI.selectOptionByValue(findTestObject('Object Repository/Forms/SSCP/Page_IT Security Certification  SSC/select_CountryAfghanistanAland'), 
    'Brazil', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Forms/SSCP/Page_IT Security Certification  SSC/select_Select Training OptionO'), 
    'Official Individual Training For You', true)

WebUI.click(findTestObject('Object Repository/Forms/SSCP/Page_IT Security Certification  SSC/input_spageurl_btn  btn-defaul'))

WebUI.verifyElementPresent(findTestObject('Forms/SSCP/SSCP Thank You'), 0)

WebUI.closeBrowser()

