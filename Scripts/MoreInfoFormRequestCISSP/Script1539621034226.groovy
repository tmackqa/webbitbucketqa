import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://wwwqa.isc2.org/Certifications/CISSP')

WebUI.setText(findTestObject('Object Repository/Forms/CISSP/Page_Cybersecurity Certification CI/input_First name_wffmf65102418'), 
    'QAAutomationCISSPLeadFN')

WebUI.setText(findTestObject('Object Repository/Forms/CISSP/Page_Cybersecurity Certification CI/input_Last name_wffmf651024180'), 
    'QAAutomationCISSPLeadLN')

WebUI.setText(findTestObject('Object Repository/Forms/CISSP/Page_Cybersecurity Certification CI/input_Phone number_wffmf651024'), 
    '813-123-4567')

WebUI.setText(findTestObject('Object Repository/Forms/CISSP/Page_Cybersecurity Certification CI/input_Email address_wffmf65102'), 
    'QAAutomationCISSPLead@mailinator.com')

WebUI.setText(findTestObject('Object Repository/Forms/CISSP/Page_Cybersecurity Certification CI/input_Company_wffmf65102418010'), 
    'QAAutomationCISSPLeadCompany')

WebUI.selectOptionByValue(findTestObject('Object Repository/Forms/CISSP/Page_Cybersecurity Certification CI/select_CountryAfghanistanAland'), 
    'Congo', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Forms/CISSP/Page_Cybersecurity Certification CI/select_Select Training OptionO'), 
    'Official Group Training For Your Team', true)

WebUI.click(findTestObject('Object Repository/Forms/CISSP/Page_Cybersecurity Certification CI/input_spageurl_btn  btn-defaul'))

WebUI.verifyElementPresent(findTestObject('Forms/CISSP/CISSP Thank You'), 0)

WebUI.closeBrowser()

