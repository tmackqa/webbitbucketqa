import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import java.sql.RowId as RowId
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

for (def row = 1; row <= findTestData('EmailsInDifferntformats').getRowNumbers(); row++) {
    WebUI.openBrowser('')

    WebUI.navigateToUrl('https://wwwqa.isc2.org/')

    WebUI.click(findTestObject('Object Repository/Page_Cybersecurity and IT Security/a_Sign In'))

    WebUI.setText(findTestObject('Object Repository/Page_/input_Required_Email'), findTestData('EmailsInDifferntFormats').getValue(
            'email', row))

    WebUI.setEncryptedText(findTestObject('Object Repository/Page_/input_Required_Password'), 'p4y+y39Ir5PEPmX20UxFKw==')

    WebUI.click(findTestObject('Object Repository/Page_/button_SIGN IN'))

    WebUI.verifyTextPresent('Hi', true)

    WebUI.click(findTestObject('Object Repository/Page_Cybersecurity and IT Security/a_Hi Name'))

    WebUI.click(findTestObject('Page_Cybersecurity and IT Security/a_Sign Out'))

    WebUI.closeBrowser()
}

